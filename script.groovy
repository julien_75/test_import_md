/* groovylint-disable CompileStatic, UnnecessaryGetter, VariableName, VariableTypeRequired */
/* groovylint-disable-next-line CompileStatic */

@Grab(group='org.codehaus.groovy.modules.http-builder', module='http-builder', version='0.7' )
import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.*
import groovyx.net.http.ContentType;

import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.util.ArrayList;
import java.util.List;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import groovy.json.JsonSlurper;
import groovyx.net.http.RESTClient;

import groovyx.net.http.HTTPBuilder;

import groovyx.net.http.HttpResponseException;

int gitLabProjectId = ; 
String file = 'test.md';
String title  = 'Titre Test';
String branch = 'master';


def getMarkdownFromGitlab (gitLabProjectId, file, branch){
    try{
        getRawText = new URL('https://gitlab.com/api/v4/projects/'+ gitLabProjectId +
                        '/repository/files/'+file+
                        '/raw?ref='+ branch)
                        .openConnection();


        getRC = getRawText.getResponseCode();

        if (getRC == 200) {
            mdText = getRawText.getInputStream().getText();
            return mdText;
                        }
        else {
            error = 'Error while accessing the group details! code=' + getRC
            return error;
        }
    } catch(e){
        print('GIT_LAB_Request_error:' + e)
    }
}


def publishOnMedium(mdText, title){

        String ACCESS_TOKEN =''; // integration token on Medium


// GET USER_ID
    try{
           def http = new HTTPBuilder('https://api.medium.com/v1/me')

            http.request(GET,TEXT) { req ->
                headers['ContentType'] = "application/json"
                headers['Authorization'] = "Bearer $ACCESS_TOKEN"
 
                response.success = { resp, reader ->
                    assert resp.status == 200 
                    println "${reader}" // get the Id to send to the POST request to publish on Medium
                }

                response.'404' = { resp ->
                      println 'Not found'
                 }
            }

    } catch(e){
        print('GET USER_ID error:' + e)
    }

// PUBLISH ARTICLE
            //prodivde  the ID get by the get request and put it in the URL (replace {{}})
          def http = new HTTPBuilder('https://api.medium.com/v1/users/{{}}/posts')

           http.request (POST, JSON){req->
            body = [
                    "title": "test",
                    "contentFormat": "markdown",
                    "content": "yooo",
                    "canonicalUrl": "http://test.com",
                    "tag":["top"],
                    "publishStatus":"draft"
                    ]
 
        // headers.'X-Application' = this.applicationKey
        // headers.'X-Authentication' = this.sessionToken
 
        response.success = { resp, json ->
            println "success: ${json}"
        }
 
        response.failure = { resp ->
            println "Unexpected error: ${resp.statusLine.statusCode}"
            println ${resp.statusLine.reasonPhrase}
        }
    }

}

// def publishOnWordPress(){

// }

String result = getMarkdownFromGitlab(projectId, file, branch);
publishOnMedium(result,title);
 // print(result)
